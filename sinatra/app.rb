require 'sinatra'

class App < Sinatra::Base
  get "/" do
    "Hola mundo"
  end

  run! if app_file == $0
end