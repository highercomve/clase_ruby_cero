# def ejecuta_bloque
#   puts "Comienza ejecucion"
#   yield
#   puts "Termina ejecucion"
# end

def ejecuta_bloque(&mi_codigo)
  puts "Comienza ejecucion"
  mi_codigo.call
  puts "Termina ejecucacion"
end

users = [
  {nombre: "Sergio", cedula: "17264103"},
  {nombre: "Alejandro", cedula: "123123"},
  {nombre: "Jose", cedula: "43278"},
  {nombre: "Jaime", cedula: "123"},
  {nombre: "Erick", cedula: "354345"},
  {nombre: "Pedro", cedula: "879789"},
  {nombre: "Ramon", cedula: "345345"},
  {nombre: "Ciriaco", cedula: "34575676"}
]

nombres = users.map do |user|
  user[:nombre]
end 

puts  nombres.inspect

valores = (1..100).map do |x|
  limite = x-1
  (2..limite).select{ |y| x % y == 0}.size 
end

puts valores.map.with_index(1){|x, i| "#{i} es primo" if x==0}.compact.inspect
