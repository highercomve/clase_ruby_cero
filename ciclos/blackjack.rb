
@mazo = (1..10).map do |carta| 
  (carta == 10) ? 12.times.map{carta} : 4.times.map{carta}
end.flatten.shuffle

@alguien_gano = false

puts "Numero de Jugadores:"
players_number = gets.chomp.to_i

players = (1..players_number).map do |player|
  puts "Coloque el nombre del jugardor #{player}"
  name = gets.chomp
  { nombre: name, cartas: [], pide: '' }
end

def repartir_cartas(player, cantidad)
  cantidad.times.each { player[:cartas].push(@mazo.pop) }
end

def sumar_cartas(player)
  player[:cartas].inject(:+)
end

puts "Repartir Cartas"
players.each do |player|
  repartir_cartas(player, 2)
end

def fin_del_turno(player)
  sumar_cartas(player) > 21 || !player[:pide].empty? || @alguien_gano
end


puts "Vamos a Jugar, turno por turno"
players.each do |player|
  puts "Es el turno de: #{player[:nombre]}"
  until fin_del_turno(player)
    puts "Esta es tu mano actual:"
    puts player[:cartas].inspect
    puts sumar_cartas(player)
    if sumar_cartas(player) == 21
      @alguien_gano = true
    else
      puts "Quieres una mas: "
      player[:pide] = gets.chomp
      repartir_cartas(player, 1) if player[:pide].empty?
    end
    puts "Esta es tu mano actual:"
    puts player[:cartas].inspect
  end
end

puts "Las cartas finales"
players.each do |player|
  puts "Cartas de #{player[:nombre]}"
  puts player[:cartas].inspect
end 