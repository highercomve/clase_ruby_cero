
# Juego de blackjack

user = []
dealer = [] 

def repartir_cartas(n, user, dealer)
  puts "Repartiendo cartas"
  i = 0
  while i < n
    user.push(rand(6) + 1)
    dealer.push(rand(6) + 1)
    i += 1
  end
end

def sumar_cartas(cartas)
  suma = 0
  # while cartas.size > 0 
  #   suma += cartas.pop
  # end 
  for carta in cartas
    suma += carta
  end
  return suma
end

def mostrar_cartas(user, dealer)
  puts user.inspect
  puts dealer.inspect
end

user_pide = ''
turno = 1

def usuario_no_gano(user_pide, user)
  user_pide.empty? && sumar_cartas(user) < 21
end

def dealer_no_gano(dealer)
  sumar_cartas(dealer) < 21
end

def nadie_gano(user, dealer)
  sumar_cartas(dealer) != 21 && sumar_cartas(user) != 21
end

while usuario_no_gano(user_pide, user) &&
      dealer_no_gano(dealer) &&
      nadie_gano(user, dealer)  

  puts "Turno: #{turno}"
  puts " "

  if turno > 1 
    repartir_cartas(1, user, dealer)
  else
    repartir_cartas(2, user, dealer)
  end
  mostrar_cartas(user, dealer)

  puts "Quiere pedir otra carta?"
  puts "Pulse ENTER para SI"
  puts "Cualquier letra para NO"
  user_pide = gets.chomp
  turno += 1
  # puts user_pide.size
  # puts user_pide.empty?
end

puts "Los resultados son: "

puts "Usuario: #{sumar_cartas(user)}"
puts "Dealer: #{sumar_cartas(dealer)}"

if (sumar_cartas(user) <= 21) && 
  (
    (sumar_cartas(user) > sumar_cartas(dealer)) || 
    sumar_cartas(dealer) > 21
  )
  puts "Ganaste el mio!!"
else
  puts "Intente de nuevo"
  puts "Inserte otra moneda"
  puts "Son $5 cada moneda"
end
