
class Player
  # attr_reader :nombre
  # attr_writer :nombre
  attr_reader :nombre
  attr_reader :dinero
  attr_accessor :cartas

  def initialize(nombre, dinero)
    @nombre = nombre
    @cartas = []
    @dinero = dinero
  end

  # def nombre
  #   @nombre
  # end

  # def nombre=(nombre)
  #   @nombre = nombre
  # end

end

class Mazo

  attr_reader :cartas
  
  def initialize
    @cartas = (1..10).map do |carta| 
      (carta == 10) ? 12.times.map{carta} : 4.times.map{carta}
    end.flatten
  end

  def barajear
    @cartas = @cartas.shuffle
  end

  def sacar_carta
    @cartas.pop
  end
end

class Play
  attr_accessor :players
  attr_reader :mazo

  def initialize
    puts "Cuantos jugadores existen:"
    @players_number = gets.chomp.to_i
    @players = []
    @mazo = Mazo.new
    @mazo.barajear
    crear_players
    repartir_a_todos(2)
  end

  def crear_players
    @players_number.times.each do |x|
      puts "Coloque el nombre del player #{x}: "
      nombre = gets.chomp
      puts "Coloque cuanta plata tiene: "
      dinero = gets.chomp
      @players << Player.new(nombre, dinero)
    end
  end

  def repartir_a_todos(n)
    @players.each do |player|
      n.times.each { repartir_a(player) }    
    end
  end

  def repartir_a(player)
    player.cartas << @mazo.sacar_carta
  end
end

