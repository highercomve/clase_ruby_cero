require_relative '../config/routes.rb'
require_relative './base_view.rb'

class BaseController

  @@routes = []
  include Routes
  include View

  def call(env)
    @env = env
    @request = Rack::Request.new(env)
    @method = @request.request_method.downcase
    @route = @request.path_info
    cargar_rutas
    enrutar
  end

  def config(method, route, action)
    @@routes << [method, route, action] 
  end

  def enrutar
    call = @@routes.select do |r| 
      r[0] == @method && r[1] == @route 
    end
    if call.size > 0
      action = call.first[2].to_sym
      send action
    else
      send :not_found
    end
  end

end