
 module View
  def render(url_vista, data, status = 200) 
    input =  File.read("vistas/#{url_vista}")
    vista = Erubis::Eruby.new(input)
    [
      status,
      {'Content-Type' => 'text/html'},
      [vista.result(data)]
    ]
  end
 end