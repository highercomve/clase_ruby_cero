require 'erubis'
require 'rest_client'
require 'json'
require 'data_mapper'

require_relative './config/database.rb'
require_relative './libs/base_controller.rb'

class User 
  include DataMapper::Resource

  property :id, Serial
  property :nombre, String
  property :apellido, String
  property :cedula, String

  has n, :books

  validates_presence_of :nombre, :apellido, :cedula
  validates_uniqueness_of :cedula
end

class Book
  include DataMapper::Resource

  property :id, Serial
  property :nombre, String
  property :autor, String

  belongs_to :user
end

class App < BaseController
  
  USERS = [
    {nombre: "Sergio Marin", cedula: "17264103"},
    {nombre: "Alejandro Marin", cedula: "16123456"},
    {nombre: "Pero Zambrano", cedula: "14025369"},
    {nombre: "Jose Paradas", cedula: "19589641"},
    {nombre: "Erick Hernandez", cedula: "18369852"}
  ]

  def index
    data = {
      titulo: "Hola este es el titulo",
      parrafo: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor eveniet veniam ea! Sequi eligendi illum eaque, dicta minima nisi cupiditate ipsa, error veniam aliquam quasi amet numquam ab unde. Unde."
    }
    render( "index.html", data )
  end

  def not_found
    render "not_found.html", {}, 404
  end

  def users
    render "users.html", {users: USERS}  
  end

  def cursos
    respuesta = RestClient.get 'https://www.escuelaweb.net/courses.json' 
    cursos = JSON.parse(respuesta.to_str)
    render 'cursos.html', { cursos: cursos }
  end

  def user(nombre)

  end
end