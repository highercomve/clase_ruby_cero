
dev_db = File.expand_path("../../db/development.db", __FILE__)

DataMapper::Logger.new($stdout, :debug)
DataMapper.setup(:default, "sqlite://#{dev_db}")