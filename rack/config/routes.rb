
module Routes

  def cargar_rutas
    config 'get', '/', 'index'
    config 'get', '/users', 'users'
    config 'get', /\/users\/.*/, 'user'
    config 'get', '/cursos', 'cursos'
  end

end 