require 'uri'

class App
  def call(env)
    @env = env
    @request = Rack::Request.new(env)
    [
      '200',
      {'Content-Type' => 'text/html'},
      ["<h1>#{@request.request_method}</h1>
        <h2>#{URI.decode(@request.path_info)}</h2>
      "]
    ]
  end
end

run App.new