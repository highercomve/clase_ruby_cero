require File.expand_path(File.dirname(__FILE__) + '/../test_config.rb')

describe "Tuit Model" do

  before do
    User.destroy_all
    Tuit.destroy_all
  end

  it 'can construct a new instance' do
    @tuit = Tuit.new
    refute_nil @tuit
  end

  it "Obligatoriamente debe tener mensaje y user_id" do
    tuit = Tuit.new
    tuit.valid?.must_equal false
    tuit = Tuit.new(mensaje: "Hola este es mi primer tuit")
    tuit.valid?.must_equal false
    tuit = Tuit.new(user_id: 1)
    tuit.valid?.must_equal false
    tuit = Tuit.new(
      mensaje: "Hola este es mi primer tuit",
      user_id: 1
    )
    tuit.valid?.must_equal true
  end

  it "El mensaje no puede tener mas de 140 caracteres" do
    tuit = Tuit.new(
      user_id: 1,
      mensaje: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia fuga quam, iste eius illo, excepturi aperiam modi, aliquid explicabo ut hic assumenda esse inventore porro libero expedita? Vel soluta eligendi enim blanditiis sint vero dignissimos amet tempora ex voluptatum perspiciatis dolor, eaque maiores doloribus provident officia accusantium necessitatibus? Repudiandae saepe est inventore, consequatur hic qui molestiae blanditiis quidem beatae tenetur vitae dolorem cumque nisi cum eaque eos! At rem, enim dolor mollitia dolorum temporibus tenetur repudiandae beatae nulla sunt officia, necessitatibus dolore neque eaque itaque autem doloribus repellendus saepe eveniet illo officiis debitis pariatur! Dolorum sapiente ullam recusandae deleniti, praesentium molestias libero perferendis optio asperiores suscipit perspiciatis architecto deserunt, sed doloribus dolor debitis quasi iste impedit hic totam incidunt! Saepe eveniet pariatur exercitationem nobis quos architecto porro beatae, quidem magni minima, rerum odit sequi explicabo dolor tempore temporibus voluptatum, soluta ex voluptates unde distinctio modi esse. Ad ipsa, tenetur, quis officiis officia odio accusamus adipisci, voluptas laboriosam modi dolore natus."
    )
    tuit.valid?.must_equal false
    tuit = Tuit.new(
      user_id: 1,
      mensaje: "Lorem ipsum dolor sit."
    )
    tuit.valid?.must_equal true
  end

end