require File.expand_path(File.dirname(__FILE__) + '/../test_config.rb')

describe "User Model" do

  before do
    User.destroy_all
  end

  it 'can construct a new instance' do
    @user = User.new
    refute_nil @user
    @user.new_record? == true
  end

  it "Puedo crear un objeto solo con nombre, apellido y email" do
    @user = User.new({ name: "Sergio",
                       last_name: "Marin", 
                       email: "higher.vnf@gmail.com" 
                    })
    @user.valid?.must_equal true
    @user = User.new({ name: "Sergio",
                       email: "higher.vnf@gmail.com" 
                    })
    @user.valid?.must_equal false
    @user = User.new({ name: "Sergio" 
                    })
    @user.valid?.must_equal false
    @user = User.new({ email: "higher.vnf@gmail.com" 
                    })
    @user.valid?.must_equal false
    @user = User.new({ last_name: "Marin", 
                       email: "higher.vnf@gmail.com" 
                    })
    @user.valid?.must_equal false
  end

  it "Nombre no puede tener menos de 6 caracteres" do
    @user = User.new(name: "Sergi")
    @user.valid?.must_equal false
    @user.errors.messages[:name].must_include "is too short (minimum is 6 characters)"
    @user = User.new(name: "Sergio")
    @user.valid?.must_equal false
    @user.errors.messages[:name].must_equal nil
  end

  # expresion regular para validar correo
  # usar la regla de validates format
  # /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  # http://guides.rubyonrails.org/active_record_validations.html
  it "El correo debe ser un correo valido" do
    user = User.new(email: "micorreofeo")
    user.valid?.must_equal false
    user.errors.messages[:email].must_equal ["Esto no es un correo"]
    user = User.new(email: "higher.vnf@gmail.com")
    user.valid?.must_equal false
    user.errors.messages[:email].must_equal nil
  end

  # validacion de uniqueness
  it "El correo no puede estar repetido en la base de datos" do
    user1 = User.create({ name: "Sergio",
                       last_name: "Marin", 
                       email: "higher.vnf@gmail.com" 
                    })
    user1.valid?.must_equal true
    user2 = User.create({ name: "Pedro",
                       last_name: "Perez", 
                       email: "higher.vnf@gmail.com" 
                    })
    user2.valid?.must_equal false
    user2.errors.messages[:email].must_equal ["El correo ya ha sido usado"]
  end

  it "El usuario debe ser mayor a 18 años" do
    user = User.new({ 
                        name: "Pedro1234",
                        last_name: "Marin", 
                        email: "higher.vnf@gmail.com",
                        fecha_nacimiento: "04/10/2014"
                    })
    user.valid?.must_equal false
    user = User.new({ 
                        name: "Pedro1234",
                        last_name: "Marin", 
                        email: "higher.vnf@gmail.com",
                        fecha_nacimiento: "10/04/1985"
                    })
    user.valid?.must_equal true
  end
  
end
