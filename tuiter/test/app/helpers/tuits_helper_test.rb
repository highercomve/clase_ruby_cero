require File.expand_path(File.dirname(__FILE__) + '/../../test_config.rb')

describe "Tuiter::App::TuitsHelper" do
  before do
    @helpers = Class.new
    @helpers.extend Tuiter::App::TuitsHelper
  end

  def helpers
    @helpers
  end

  it "should return nil" do
    assert_equal nil, helpers.foo
  end
end
