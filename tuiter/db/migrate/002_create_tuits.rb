class CreateTuits < ActiveRecord::Migration
  def self.up
    create_table :tuits do |t|
      t.string :mensaje
      t.references :user
      t.timestamps
    end
  end

  def self.down
    drop_table :tuits
  end
end
