class AddFechaNacimientoToUser < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.date :fecha_nacimiento
    end
  end

  def self.down
    change_table :users do |t|
      t.remove :fecha_nacimiento
    end
  end
end
