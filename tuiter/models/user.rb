class User < ActiveRecord::Base
  validates :name, :last_name, :password, :email, presence: true 
  validates :name, length: { minimum: 6 }
  validates :password, length: { minimum: 8}
  validates :email, format: { 
    with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,
    message: "Esto no es un correo"
  }
  validates :email, uniqueness: { message: "El correo ya ha sido usado" }

  validates_each :fecha_nacimiento do |record, attr, value|
    unless value.nil?
      record.errors.add(
        attr,
        "No eres mayor a 18 años"
      ) if (value >= Date.today - 18.year)
    end
  end

  has_many :tuits
  has_many :friends, class_name: "Friendship"
  has_many :followers, 
            class_name: "Friendship", 
            inverse_of: :friend, 
            foreign_key: "friend_id"
  
  has_many :mentions
  
  # has_many  :friends, 
  #           through: :friendships

  # has_many :followers, 
  #           through: :friendships, 
  #           class_name: "User",
  #           foreign_key: "user_id"
  # before_save
  # after_save
  # before_validate
  # after_validate
  # before_create
  # after_create
  # before_destroy
  # after_destroy

  before_save :convertir_password
  def self.mention_tuits
    Mention.includes([:tuit]).
            where(user_id: @user.id).
            map{ |m| m.tuit }
  end
  def self.validate_login(email, password)
    User.where({
      email: email,
      password: Digest::SHA512.base64digest(password)
    }).first
  end

  def edad
    (Date.today - fecha_nacimiento).to_i / 365
  end

  def full_name
    "#{name} #{last_name}"
  end

  private
  def convertir_password
    self.password = Digest::SHA512.base64digest(self.password)
  end

  def validar_password(texto)
    password == Digest::SHA512.base64digest(texto)
  end
end
