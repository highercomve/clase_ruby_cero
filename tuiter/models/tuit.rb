class Tuit < ActiveRecord::Base
  belongs_to :user
  has_many :replies, 
            class_name: "Tuit",
            foreign_key: "parent_id"
  
  has_many :mentions
  
  accepts_nested_attributes_for :mentions

  belongs_to :parent, class_name: "Tuit"

  validates :mensaje, :user_id, presence: true
  validates :mensaje, length: { maximum: 140 }

  scope :lastest, -> { order('created_at DESC') }

  before_save :extract_mentions

  def self.timeline(user)
    ids = [user.id]
    if  user.friend_ids.size > 0
      ids << user.friend_ids
    end
    Tuit.includes([:replies]).where(user_id: ids).where("tuits.parent_id IS NULL").order('created_at DESC')
  end

  def extract_mentions
    usernames = mensaje.scan(/@\w*/i).map {|s| s[1..-1]}
    users = User.where(username: usernames)
    self.mentions_attributes = users.map do |user| 
      {
        tuit_id: self.id,
        user_id: user.id 
      } 
    end
  end 
end
