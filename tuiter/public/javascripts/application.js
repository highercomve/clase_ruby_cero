// Put your application scripts here

jQuery.timeago.settings.strings = {
   prefixAgo: "hace",
   prefixFromNow: "dentro de",
   suffixAgo: "",
   suffixFromNow: "",
   seconds: "menos de un minuto",
   minute: "un minuto",
   minutes: "unos %d minutos",
   hour: "una hora",
   hours: "%d horas",
   day: "un día",
   days: "%d días",
   month: "un mes",
   months: "%d meses",
   year: "un año",
   years: "%d años"
};

(function($){
  $(function() {
    $("span.timeago").timeago();
    var mensajes = $(".tuit-mensaje");
    mensajes.each(function() {
      var $this = $(this);
      var users = $this.text().match(/@\w*/g);
      console.log(users);
      if(users) {
        users.forEach(function(user, index, array) {
          user = user.replace("@",'');
          console.log(user);
          var new_mensaje = $this.html().replace(user, "<a href='/u/"+user+"'>"+user+"</a>");
          $this.html(new_mensaje);
          console.log($this.html());
        });
      }
    });
  });
})(jQuery);