# Helper methods defined here can be accessed in any controller or view in the application

module Tuiter
  class App
    module TuitsHelper
      # def simple_helper_method
      # ...
      # end
    end

    helpers TuitsHelper
  end
end
