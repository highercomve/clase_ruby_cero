Tuiter::App.controllers :tuits do
  
  # get :index, :map => '/foo/bar' do
  #   session[:foo] = 'bar'
  #   render 'index'
  # end

  # get :sample, :map => '/sample/url', :provides => [:any, :js] do
  #   case content_type
  #     when :js then ...
  #     else ...
  # end

  # get :foo, :with => :id do
  #   'Maps to url '/foo/#{params[:id]}''
  # end

  # get '/example' do
  #   'Hello world!'
  # end

  before except: :show do
    unless autenticado?
      redirect_to '/login'
    end
  end
  
  get :index, :map => '/' do
    render "tuits/index"
  end

  get :new do
    @tuit = current_user.tuits.build
    unless params[:parent_id].nil?
      @to_reply = Tuit.find(params[:parent_id]) 
      params[:mensaje] = "@#{@to_reply.user.username}" + params[:mensaje].to_s 
    end
    render "tuits/new"
  end

  post :create do
    @tuit = current_user.tuits.build({
      mensaje: params[:mensaje],
      parent_id: params[:parent_id]
    })
    if @tuit.save
      redirect_to '/'
    else
      render "tuits/new"
    end
  end

  get :show do

  end

end
