Tuiter::App.controllers :sessions do
  
  # get :index, :map => '/foo/bar' do
  #   session[:foo] = 'bar'
  #   render 'index'
  # end

  # get :sample, :map => '/sample/url', :provides => [:any, :js] do
  #   case content_type
  #     when :js then ...
  #     else ...
  # end

  # get :foo, :with => :id do
  #   'Maps to url '/foo/#{params[:id]}''
  # end

  # get '/example' do
  #   'Hello world!'
  # end
  
  get :new, :map => "/login" do
    @user = User.new
    render 'sessions/new'
  end

  post :create , :map => '/login' do
    @user = User.validate_login(params[:email], params[:password])
    if @user.nil?
      "No exite el usario"
    else
      session[:user] = @user
      redirect_to '/'
    end
  end

  get :destroy, :map => '/logout' do
    session[:user] = nil
    redirect_to '/'
  end

end
