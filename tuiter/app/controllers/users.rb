require "prawn"

Tuiter::App.controllers :users do
  layout :application 

  get :index do
    @usuarios = User.all
    render 'users/index'
  end

  get :show, map: '/u/:id' do
    @user = User.where(username: params[:id]).first ||
            User.where(id: params[:id]).first
    unless @user.nil?
      render "users/show"
    else
      "Not found"
    end
  end

  get :mentions, with: :id do
    @user = User.find(params[:id])
    @tuits = @user.mention_tuits
    render "users/mentions" 
  end

  get :show, with: :id, provides: [:pdf, :html, :xml, :json] do
    @user = User.where(username: params[:id]).first ||
            User.where(id: params[:id]).first
    unless @user.nil?
      # case content_type
      #   when :html  then 

      #   end
      #   when :json  then ... end
      # end
      if content_type == :pdf
        content_type 'application/pdf'
        pdf = Prawn::Document.new
        pdf.text "Hello Anynines, Prawn and Sinatra"
        pdf.move_down 20
        pdf.text "#{@user.name}"
        pdf.render
        # PDFKit.new('https://www.escuelaweb.net')
      else  
        render "users/show"
      end
    else
      "Not found"
    end
  end

  get :new do
    @user = User.new
    render "users/register"
  end

  post :create do
    @user = User.new(
      name: params[:name],
      last_name: params[:last_name],
      email: params[:email],
      password: params[:password],
      username: params[:username],
      fecha_nacimiento: params[:fecha_nacimiento]
    )
    if @user.save
      render 'users/created'
    else
      render "users/register"
    end
  end

  get :edit, with: :id  do
    @user = User.find(params[:id])
    render 'users/edit'
  end

  get :follow, with: :id do
    friend = current_user.friends.new(friend_id: params[:id])
    puts friend.inspect
    if friend.save
      redirect_to "/u/#{friend.friend.username}"
    else

    end
  end 

  put :edit, with: :id do
    @user = User.find(params[:id])
    new_attributes = {
      name: params[:name],
      last_name: params[:last_name],
      email: params[:email],
      username: params[:username],
      fecha_nacimiento: params[:fecha_nacimiento]
    }
    if @user.update_attributes(new_attributes)
      redirect_to "users/show/#{@user.id}"
    else
      render 'users/edit'
    end
  end


  # get :index, :map => '/foo/bar' do
  #   session[:foo] = 'bar'
  #   render 'index'
  # end

  # get :sample, :map => '/sample/url', :provides => [:any, :js] do
  #   case content_type
  #     when :js then ...
  #     else ...
  # end

  # get :foo, :with => :id do
  #   'Maps to url '/foo/#{params[:id]}''
  # end

  # get '/example' do
  #   'Hello world!'
  # end
  

end
